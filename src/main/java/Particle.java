import java.util.UUID;

public class Particle implements Cloneable
{
    UUID id;
    private final Tupel3d p;
    private final Tupel3d v;
    private final double mass;
    private final Tupel3d color;

    private final Object cs = new Object();

    public Particle(double x, double y, double z, double vx, double vy, double vz, double m) {
        id = UUID.randomUUID();
        synchronized(cs) {
            p = new Tupel3d(x,y,z);
            v = new Tupel3d(vx,vy,vz);
            this.mass = m;
        }
        color = new Tupel3d(Math.random() * 255 + 1,Math.random() * 255 + 1, Math.random() * 255 + 1);
    }

    public Particle(Particle other) {
        synchronized(cs) {
            this.id = other.id;
            this.p = new Tupel3d(other.p);
            this.v = new Tupel3d(other.v);
            this.mass = other.mass;
            this.color = new Tupel3d(other.color);
        }
    }

    public Tupel3d getP() {
        synchronized(cs) {
            return new Tupel3d(p);
        }
    }

    public double getMass() {
        synchronized(cs) {
            return mass;
        }
    }
    public Tupel3d getColor()
    {
        synchronized(cs) {
            return color;
        }
    }

    public void tick(double dt) {
        synchronized (cs) {

            double distance_to_left = p.x;
            double distance_to_top = p.y;

            double s = 2.0; // (a+b)^2 - Euklid Metrik

            double force_to_right = Math.pow( 1/distance_to_left, s);

            double distance_to_right = Application.width - p.x;
            double force_to_left = -Math.pow(1.0/distance_to_right, s);


            double distance_to_bottom = Application.height - p.y;

            double force_to_bottom = Math.pow(1.0/distance_to_top, s);
            double force_to_top = -Math.pow(1.0/distance_to_bottom, s);

            double forcex = (force_to_left + force_to_right)/mass;
            double forcey = (force_to_bottom + force_to_top)/mass;

            v.x += forcex;
            v.y += forcey;

            p.scaleAdd(dt, v, p);

            if (p.x > Application.width )
            {
                p.x = Application.width - 1;
                System.out.println(String.format("Overflow in p=(%f,%f), v=(%f, %f)\n", p.x, p.y, v.x, v.y));
            }
            else if( p.x <= 0 )
            {
                p.x = 0;
                System.out.println(String.format("Overflow in p=(%f,%f), v=(%f, %f)\n", p.x, p.y, v.x, v.y));
            }
        }
    }

    public Tupel3d[] getPVC() {
        Tupel3d[] arr = new Tupel3d[3];
        synchronized (cs){
            arr[0] = new Tupel3d(p);
            arr[1] = new Tupel3d(v);
            arr[2] = new Tupel3d(color);
        }

        return arr;
    }

    public void force(Particle other) {
        if( this.equal(other))
        {
            return;
        }

        Tupel3d dv = new Tupel3d(this.getP().x - other.getP().x,
                                this.getP().y - other.getP().y,
                                this.getP().z - other.getP().z);
        double distance = dv.getLength();

        double m1 = this.getMass();
        double m2 = other.getMass();

        double G = 0.1;
        double f = -G*m1*m2/(distance*distance);
        dv.scale(f);
        v.add(dv);
    }

    private boolean equal(Particle other) {
        return this.id == other.id;
    }
}
