import java.lang.Math;

import static java.lang.Math.abs;

public class Tupel3d
{
    public  double  x;
    public  double  y;
    public  double  z;

    // Construct with (x,y,z)
    public Tupel3d(double x, double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    // Construct with (v[])
    public Tupel3d(double[] t)
    {
        this.x = t[0];
        this.y = t[1];
        this.z = t[2];
    }

    // Construct with (other)
    public Tupel3d(Tupel3d o)
    {
      this.x = o.x;
      this.y = o.y;
      this.z = o.z;
    }

    // Construct from (0,0,0).
    public Tupel3d()
    {
        this.x = 0.0;
        this.y = 0.0;
        this.z = 0.0;
    }

    // set(x,y,z)
    public final void set(double x, double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    // set( v[] )
    public final void set(double[] t)
    {
        this.x = t[0];
        this.y = t[1];
        this.z = t[2];
    }

    // set from ( other )
    public final void set(Tupel3d other)
    {
      this.x = other.x;
      this.y = other.y;
      this.z = other.z;
    }

    // get to ( target[] )
    public final void get(double[] t)
    {
        t[0] = this.x;
        t[1] = this.y;
        t[2] = this.z;
    }

    // get to ( target )
    public final void get(Tupel3d target)
    {
        target.x = this.x;
        target.y = this.y;
        target.z = this.z;
    }

    // this = sum(a, b)
    public final void add(Tupel3d t1, Tupel3d t2)
    {
        this.x = t1.x + t2.x;
        this.y = t1.y + t2.y;
        this.z = t1.z + t2.z;
    }

    // add from( other )
    public final void add(Tupel3d other)
    { 
        this.x += other.x;
        this.y += other.y;
        this.z += other.z;
    }

    // this = sub(b, a) = a - b;
    public final void sub(Tupel3d a, Tupel3d b)
    {
        this.x = a.x - b.x;
        this.y = a.y - b.y;
        this.z = a.z - b.z;
    }
 
    // this = this - other
    public final void sub(Tupel3d other)
    { 
        this.x -= other.x;
        this.y -= other.y;
        this.z -= other.z;
    }

    // this = negative of other
    public final void negate(Tupel3d t1)
    {
        this.x = -t1.x;
        this.y = -t1.y;
        this.z = -t1.z;
    }

    // negate inplace
    public final void negate()
    {
        this.x = -this.x;
        this.y = -this.y;
        this.z = -this.z;
    }

    // this = scale * other;
    public final void scale(double s, Tupel3d other)
    {
        this.x = s*other.x;
        this.y = s*other.y;
        this.z = s*other.z;
    }


    // scale inline
    public final void scale(double s)
    {
        this.x *= s;
        this.y *= s;
        this.z *= s;
    }


    // this = scale * a + b
    public final void scaleAdd(double s, Tupel3d t1, Tupel3d t2)
    {
        this.x = s*t1.x + t2.x;
        this.y = s*t1.y + t2.y;
        this.z = s*t1.z + t2.z;
    }

    // inline this = scale * this + other
    public final void scaleAdd(double s, Tupel3d other) {
        this.x = s*this.x + other.x;
        this.y = s*this.y + other.y;
        this.z = s*this.z + other.z;
    }

    // toString
    public String toString() {
        return "(" + this.x + ", " + this.y + ", " + this.z + ")";
    }

    // equal to other Tupel3d
    public boolean equals(Tupel3d other)
    {
        try {
            return(this.x == other.x && this.y == other.y && this.z == other.z);
        }
        catch (NullPointerException e2) {return false;}
    }

    // equal within epsilon
    public boolean equals(Tupel3d other, double epsilon)
    {
       double diff;

       diff = x - other.x;
       if(Double.isNaN(diff)) return false;
       if((diff<0?-diff:diff) > epsilon) return false;

       diff = y - other.y;
       if(Double.isNaN(diff)) return false;
       if((diff<0?-diff:diff) > epsilon) return false;

       diff = z - other.z;
       if(Double.isNaN(diff)) return false;
       return !( ( diff < 0 ? -diff : diff ) > epsilon );
    }

    // this = clamp(min, max(other)
    public final void clamp(double min, double max, Tupel3d t)
    {
        if( t.x > max ) { x = max; }
        else x = Math.max(t.x, min);
 
        if( t.y > max ) { y = max; }
        else y = Math.max(t.y, min);
 
        if( t.z > max ) { z = max; }
        else z = Math.max(t.z, min);
    }

    // clamp min only: this = clampmin(other)
    public final void clampMin(double min, Tupel3d t)
    {
        x = Math.max(t.x, min);

        y = Math.max(t.y, min);

        z = Math.max(t.z, min);
    } 

    // this = clampmax(mvalue, other)
    public final void clampMax(double max, Tupel3d t) {
        x = Math.min(t.x, max);

        y = Math.min(t.y, max);

        z = Math.min(t.z, max);
   } 

    // this = abs(other)
    public final void absolute(Tupel3d t)
    {
        x = abs(t.x);
        y = abs(t.y);
        z = abs(t.z);
    } 

    //  clamp this to the range [min, max].
    public final void clamp(double min, double max) {
        if( x > max ) { x = max; }
        else if( x < min ){ x = min; }
 
        if( y > max ) { y = max; }
        else if( y < min ) { y = min; }
 
        if( z > max ) { z = max; }
        else if( z < min ) { z = min; }
    }
 
    //  Clamp this to minimum
    public final void clampMin(double min) { 
        if( x < min ) x=min;
        if( y < min ) y=min;
        if( z < min ) z=min;
    } 
 
    // Clamp this to maximum 
    public final void clampMax(double max) { 
        if( x > max ) x=max;
        if( y > max ) y=max;
        if( z > max ) z=max;
    }

    // this =absolute(this)
    public final void absolute()
    {
        x = abs(x);
        y = abs(y);
        z = abs(z);
    }

    // this = linear interpolation between t1, t2
    public final void interpolate(Tupel3d t1, Tupel3d t2, double alpha) {
        this.x = (1-alpha)*t1.x + alpha*t2.x;
        this.y = (1-alpha)*t1.y + alpha*t2.y;
        this.z = (1-alpha)*t1.z + alpha*t2.z;
    }
 
    // this = linear interpolation between this and t1
    public final void interpolate(Tupel3d t1, double alpha) {
        this.x = (1-alpha)*this.x + alpha*t1.x;
        this.y = (1-alpha)*this.y + alpha*t1.y;
        this.z = (1-alpha)*this.z + alpha*t1.z;
    }
 
    // getX
    public final double getX() {
        return x;
    }

    // setX
    public final void setX(double x) {
        this.x = x;
    }

    // getY
    public final double getY() {
        return y;
    }

    // setY
    public final void setY(double y) {
        this.y = y;
    }

    // getZ
    public final double getZ() {
        return z;
    }

    // setZ
    public final void setZ(double z) {
        this.z = z;
    }

    public void normalize() {
        double length = Math.sqrt(x*x+y*y+z*z);
        if( length > -1.0/1000000000 && length < 0)
            length = -1.0/1000000000;
        else if(length >= 0 && length < 1.0/1000000000 )
            length = 1.0/1000000000;
        scale(1.0/length);
    }

    public double getLength() {
        return Math.sqrt(x*x + y*y + z*z);
    }
}
