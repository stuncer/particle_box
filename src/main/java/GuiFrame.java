import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GuiFrame extends JFrame {
    Model model;
    public GuiFrame(String title, Model model) {
        super(title);
        this.model = model;
        setLayout(new BorderLayout());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        OutputPanel outputPanel = new OutputPanel(model);
        add(outputPanel, BorderLayout.CENTER);

        JPanel cmdBar = new JPanel();
        add(cmdBar, BorderLayout.PAGE_END);

        JButton reset = new JButton("Reset All");
        reset.addActionListener(e -> {
            model.reset();
            outputPanel.reset();
        });
        cmdBar.add(reset, BorderLayout.PAGE_END);

        JButton reset_background = new JButton("Reset Background");
        reset_background.addActionListener(e -> outputPanel.reset_background());
        cmdBar.add(reset_background, BorderLayout.PAGE_END);

        JButton stop = new JButton("Start/Stop");
        stop.addActionListener(e -> {
            if (model.stopped(false)) {
                outputPanel.timerStart();
                new Thread(model).start();
            } else {
                outputPanel.timerStop();
                model.stopped(true);
            }
        });
        cmdBar.add(stop, BorderLayout.PAGE_END);
    }

    public void start() {
        pack();
        setSize(Application.width, Application.height);
        setVisible(true);
    }
}
