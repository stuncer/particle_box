import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class OutputPanel extends JPanel implements ActionListener
{
    Model model;

    private final Timer timer = new Timer(10, this);
    long frames = 0;
    long starttime = 0;

    BufferedImage background = new BufferedImage(Application.width, Application.height, BufferedImage.TYPE_INT_RGB);
    private final Color text_color = new Color(30, 140, 30, 255);

    OutputPanel(final Model model)
    {
        this.model = model;

        Graphics2D graphics = (Graphics2D) background.getGraphics();
        graphics.setBackground(new Color(255, 255, 255, 0));
        graphics.clearRect(0,0, Application.width, Application.height);

        addMouseListener(new MouseAdapter()
        {
            public void mousePressed(MouseEvent e)
            {
                if( e.getButton() == 1 ) {
                    Particle p = new Particle(
                        e.getX(), e.getY(), 0,
                        0, 0, 0,
                        1);

                    model.addParticel(p);
                    updateUI();
                }
                else if ( e.getButton() == 3 ) {
                    model.removeParticle();
                    updateUI();
                }
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        this.repaint();
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        long start = System.nanoTime();

        frames++;
        super.paintComponent(g);

        ArrayList<Particle> particles = model.getParticles().particles;

        for( Particle particle : particles )
        {
            Tupel3d[] a = particle.getPVC();

            Tupel3d p = a[0];
            Tupel3d v = a[1];
            Tupel3d color = a[2];
            double mass = particle.getMass();

            if( (int)p.x >= Application.width)
            {
                System.out.println("x overflow: " + p.x);
            }
            else if( (int)p.x < 0)
            {
                System.out.println("x underflow: " + p.x);
            }
            else if( (int)p.y >= Application.height)
            {
                System.out.println("y overflow: " + p.y);
            }
            else if (p.y < 0) {
                System.out.println("y underflow: " + p.y);
            }
            else {

                background.setRGB((int) p.x, (int) p.y, (int) (color.x*255*255 + color.y*255 + color.z));
            }
        }

        Graphics2D g2d = (Graphics2D) g.create();
        g2d.drawImage(background, 0, 0, this);
        g2d.dispose();

        int line = 1;
        for(Particle particle: model.getParticles().particles )
        {
            Tupel3d[] pv = particle.getPVC();
            Tupel3d p = pv[0];
            Tupel3d c = pv[2];

            // Tupel3d c = pv[2];
            // double m = particle.getMass();
            // g.drawString(String.format("p%d = (%d, %d, %d)", pc, (int)p.x, (int)p.y, (int)p.z), 5, 16* line++);
            // g.drawString(String.format("v%d = (%f, %f, %f)", pc, v.x, v.y, v.z), 5, 16*line++);

            g.setColor(text_color);
            g.drawArc((int) pv[0].x - 15, (int) p.y-15, 30, 30, 0, 360);

            Color color = new Color((int)c.x, (int)c.y, (int)c.z, 125); //Red
            g.setColor(color);

            g.fillOval( (int) pv[0].x-15, (int) p.y-15, 30, 30);
        }
        long end = System.nanoTime();
        double frameduration = (end - start)/1000000000d;
        double overalltime = (end - starttime)/1000000000d;

        g.setColor(text_color);
        g.drawString(String.format("%dm ticks", (long)model.getTickcount()/1000000), 5, 16*line++);
        g.drawString(String.format("%f seconds", overalltime), 5, 16*line++);
        g.drawString(String.format("%d frames", frames), 5, 16 * line++);
        g.drawString(String.format("%f tim per frm", frameduration), 5, 16 * line++);
        g.drawString(String.format("%f fps", frames/overalltime), 5, 16 * line);
    }

    public void timerStop()
    {
        timer.stop();
    }

    public void timerStart() {
        starttime = System.nanoTime();
        timer.start();
    }

    public void reset() {
        timerStop();
        reset_background();
    }

    public void reset_background() {
        Graphics2D graphics = (Graphics2D) background.getGraphics();
        graphics.setBackground(new Color(255, 255, 255, 0));
        graphics.clearRect(0,0, Application.width, Application.height);
        updateUI();
    }
}
