import java.util.ArrayList;

public class Particles
{
    ArrayList<Particle> particles = new ArrayList<>();

    public Particles(Particles particles)
    {
        for(Particle p : particles.particles)
        {
            this.append(new Particle(p));
        }
    }

    public Particles()
    {
    }

    void append(Particle other)
    {
        particles.add(other);
    }

    public void pop()
    {
        if( particles.size() > 0 )
        {
            particles.remove(particles.size() - 1);
        }
    }

    public void timestep(double difference)
    {
        particles.forEach(p1 -> particles.forEach(p1::force));
        particles.forEach(particle -> particle.tick(difference));
    }
}
