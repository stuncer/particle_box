public class Application
{
    public static final int width = 700;
    public static final int height = 700;

    private final Model model = new Model();
    private final GuiFrame gui = new GuiFrame("Particles 2D", model);

    private void start() {
        gui.start();
    }

    public static void main(String[] args)
    {
        Application app = new Application();
        app.start();
    }
}