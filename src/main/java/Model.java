public class Model implements Runnable
{
    // Runnable
    private boolean stopped = true;
    private final Object cs = new Object();

    // Particles
    private Particles particles = new Particles();
    private final Object lockParticles = new Object();

    // Timing info
    protected long time = 0;
    private int tickcount = 0;

    Model()
    {
        // particles.append(new Particle(Main.width/2, Main.height/2, 0, 400,340,0,  1));
        // particles.append(new Particle(150, Main.height/2 + 50, 0, 0,0,0, 1));
    }

    @Override
    public void run()
    {
        stopped = false;
        time = System.nanoTime();
        while( !stopped(false) )
        {
            tick();
        }
    }

    boolean stopped(boolean b) {
        synchronized(cs) {
            if (b) {
                stopped = b;
            }
            return stopped;
        }
    }

    private void tick()
    {
        synchronized(lockParticles) {
            tickcount++;
            long now = System.nanoTime();

            // wieviel zeit ist in der Welt vergangen?
            double difference_in_seconds = (now - time) / 1000000000d;
            particles.timestep(difference_in_seconds);
            time = now;
        }
    }

    int getTickcount() { return tickcount; }

    public Particles getParticles() {
        return new Particles(particles);
    }

    public void addParticel(Particle p) {
        synchronized(lockParticles) {
            particles.append( p );
        }
    }

    public void removeParticle() {
        synchronized(lockParticles)
        {
            particles.pop();
        }
    }

    public void reset() {
        synchronized(lockParticles)
        {
            time = 0;
            tickcount = 0;
            particles = new Particles();
        }
    }
}
